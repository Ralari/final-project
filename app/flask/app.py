#!/usr/bin/python3

import os
from flask import Flask, render_template, request, redirect, url_for, flash, session, escape
from flask_autoindex import AutoIndex
from werkzeug.utils import secure_filename
from simplepam import authenticate

app = Flask(__name__)
app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
home = os.path.expanduser("~")  # переменная, задающая путь к домашней папке для загрузки и просмотра файлов
files_index = AutoIndex(app, browse_root=home, add_url_rules=False)  # переменная для запуска autoindex


@app.route('/')     # если пользователь не залогинен, происходит перенаправление на страницу авторизации
def index():
    if 'username' in session:
        return render_template('index.html')
    return redirect(url_for('login'))


@app.route('/login', methods=['GET', 'POST'])  # авторизация сделана при помощи модуля simplepam
def login():                                   # https://pypi.org/project/simplepam/
    if 'username' in session:                  # для входа надо использовать логин и пароль пользователя в ОС
        return render_template('index.html')
    elif request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        if authenticate(str(username), str(password)):      # при успешной авторизации переменной session присвается
            session['username'] = request.form['username']  # значение, оно исользуется для проверки статуса авторизации
            return redirect(url_for('index'))
        else:
            return 'Invalid username/password'
    return '''
        <h1>Login</h1>
        <form action="" method="post">
            <p><input type=text name=username>
            <p><input type=password name=password>
            <p><input type=submit value=Login>
        </form>
    '''


@app.route('/logout')
def logout():
    session.pop('username', None)       # при выходе из приложения имя пользователя из session убирается
    return redirect(url_for('index'))


@app.route('/files/')
@app.route('/files/<path:path>')
def autoindex(path='.'):            # просмотр файлов при помощи autoindex
    if 'username' in session:
        return files_index.render_autoindex(path, template='autoindex.html')
    else:
        return redirect(url_for('login'))


@app.route('/upload', methods=['GET', 'POST'])
def upload():                      # загрузка файлов, код из документации flask
    if 'username' in session:
        if request.method == 'POST':
            if 'file' not in request.files:
                flash('No file part')
                return redirect(request.url)
            file = request.files['file']
            if file.filename == '':
                flash('No selected file')
                return redirect(request.url)
            if file:
                filename = secure_filename(file.filename)
                file.save(os.path.join(home, filename))
                return render_template('upload.html')
        return '''
            <!doctype html>
            <title>Upload new File</title>
            <h1>Upload new File</h1>
            <form method=post enctype=multipart/form-data>
              <input type=file name=file>
              <input type=submit value=Upload>
            </form>
            '''
    else:
        return redirect(url_for('login'))


if __name__ == '__main__':
    app.run()
